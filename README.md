# Init

## git

````
1. git submodule foreach git checkout dev
2. git submodule init
3. git submodule foreach git pull
4. git submodule update --recursive
````

## App

````
Может быть вынести в докер обновление файла
sudo chmod -R ugo+rw storage
````

## Let`s encrypt

````
02.03.2022
https://finnian.io/blog/ssl-with-docker-swarm-lets-encrypt-and-nginx/

docker run --rm \
  -p 443:443 -p 80:80 --name letsencrypt \
  -v "/home/workmanager/workmanager/data/etc/letsencrypt:/etc/letsencrypt" \
  -v "/home/workmanager/workmanager/data/var/lib/letsencrypt:/var/lib/letsencrypt" \
  certbot/certbot certonly -n \
  -m "noreplay@workmanager.pw" \
  -d beta-api.workmanager.pw \
  --standalone --agree-tos
  
docker run --rm --name letsencrypt \
    -v "/home/workmanager/workmanager/data/etc/letsencrypt:/etc/letsencrypt" \
    -v "/home/workmanager/workmanager/data/var/lib/letsencrypt:/var/lib/letsencrypt" \
    -v "/home/workmanager/workmanager/workmanager-backend:/var/www/workmanager-backend" \
    certbot/certbot:latest \
    renew --quiet
````

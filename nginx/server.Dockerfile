FROM nginx:1.21-alpine

ADD conf/nginx.conf /etc/nginx/
ADD conf/workmanager.conf /etc/nginx/
ADD conf/services/workmanager-backend.conf /etc/nginx/services/workmanager-backend.conf

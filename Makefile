dev-b-p: build-dev push-dev

build-dev:
	@docker-compose -f docker-compose.ci-dev.yml build

push-dev:
	@docker-compose -f docker-compose.ci-dev.yml push

